# syntax=docker/dockerfile:1.3
FROM registry.fedoraproject.org/fedora:35

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image for distributed building of C/C++ programs on Fedora using distcc"

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer/docker-fedora-distcc.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0

# hadolint shell=/usr/bin/bash
RUN printf "[main]\ngpg_check=1\ninstallonly_limit=2\nclean_requirements_on_remove=True\nfastestmirror=True\nmax_parallel_downloads=7\n" > /etc/dnf/dnf.conf; \
    cat /etc/dnf/dnf.conf; \
    dnf install 'dnf-command(config-manager)' -y && \
    dnf config-manager --set-enabled updates && \
    dnf config-manager --set-enabled updates-testing && \
    dnf --refresh upgrade -y
# hadolint ignore DL3059
RUN dnf install --nodocs dnf dnf-plugins-core -y && \
    dnf copr enable eddsalkield/iwyu -y && \
    dnf install --nodocs --setopt install_weak_deps=0 -y \
    git \
    ninja-build \
    make \
    {c,auto}make \
    gcc \
    gcc-c++ \
    libgcc \
    libstdc++-{devel,static} \
    glibc-devel \
    clang \
    distcc \
    distcc-server \
    iwyu \
    cryptopp-devel \
    libasan-static \
    liblsan-static \
    libubsan-static \
    libtsan-static \
    binutils \
    lld \
    flex \
    bison \
    which \
    file \
    protobuf-c-{devel,compiler} \
    protobuf-compiler \
    cppunit \
    capnproto-{devel,libs} \
    libpcap-devel \
    kernel-devel \
    && dnf clean all -y

ENTRYPOINT ["distccd", \
  "--daemon", \
  "--no-detach", \
  "--user", "distcc", \
  "--port", "3632", \
  "--stats", \
  "--stats-port", "3633", \
  "--log-stderr", \
  "--listen", "0.0.0.0"\
]

# By default the distcc server will accept clients from everywhere.
CMD ["--allow", "0.0.0.0/0", \
  "--nice", "3", \
  "--jobs", "6" \
]


